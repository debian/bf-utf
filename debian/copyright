Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source:
 This package was initially debianized by David Whedon <dwhedon@debian.org> on
 Sun, 11 Mar 2001 19:43:29 -0800.
 .
 It was generated from the boot-floppies cvs archive: cvs.debian.org
 .
  - The ucs.bdf font is public domain. We lost track of who made them first.
  - The unifont.bdf font was taken from Roman Czyborra's "proposal for a
    GNU Unicode font" at http://czyborra.com/unifont/ .
  - The Hangul (Korean) glyphs in unifont.bdf were replaced with Baekmuk Gulim
    Hangul fonts encoded in ISO-10646 by Changwoo Ryu <cwryu@debian.org>. The
    Baekmuk fonts can be found in Debian xfonts-baekmuk package.
 .
 Author: Markus Kuhn <mkuhn@acm.org> and Nicolas Sabouret <nico@debian.org>

Files: src/ucs.bdf
Copyright: unknown
License: public-domain
 COPYRIGHT "Public domain font.  Share and enjoy."

Files: src/unifont.bdf
License: GPL-2+ with font embedding exception and MIT-like-Baekmuk
Copyright: Copyright 1998-2001 Roman Czyborra, Paul Hardy, Qianqian Fang, Andrew Miller, Johnnie Weaver, David Corbett, et al.
 1986-2002 Kim Jeong-Hwan

Files: debian/*
Copyright: David Whedon, Markus Kuhn, Nicolas Sabouret and Changwoo Ryu
License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: GPL-2+ with font embedding exception
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.
 .
 As a special exception, if you create a document which uses this font,
 and embed this font or unaltered portions of this font into the
 document, this font does not by itself cause the resulting document to
 be covered by the GNU General Public License. This exception does not
 however invalidate any other reasons why the document might be covered
 by the GNU General Public License. If you modify this font, you may
 extend this exception to your version of the font, but you are not
 obligated to do so. If you do not wish to do so, delete this exception
 statement from your version.

License: MIT-like-Baekmuk
 All rights reserved.
 .
 Permission to use, copy, modify and distribute this font is
 hereby granted, provided that both the copyright notice and
 this permission notice appear in all copies of the font,
 derivative works or modified versions, and that the following
 acknowledgement appear in supporting documentation:
 .
     Baekmuk Batang, Baekmuk Dotum, Baekmuk Gulim, and
     Baekmuk Headline are registered trademarks owned by
     Kim Jeong-Hwan.
 .
 저작권 (c) 1986-2002 김정환
 .
 저작권자는 이 서체를 자유롭게 사용, 복사, 수정 및 배포할 수
 있도록 허가합니다. 단, 이 서체의 모든 사본, 파생물 및 수정본에는
 위의 저작권 문구와 본 허가 문구를 반드시 포함하여야 하며, 함께
 제공되는 문서에 다음과 같은 문구를 반드시 표시하여야 합니다:
 .
     백묵 바탕, 백묵 돋음, 백묵 굴림, 백묵 헤드라인은 김정환의
     등록상표입니다.
