bf-utf (0.08+nmu2) unstable; urgency=medium

  * Non-maintainer upload.
  * source only upload to enable migration (Closes: #997962)

 -- Paul Gevers <elbrus@debian.org>  Wed, 27 Oct 2021 22:16:17 +0200

bf-utf (0.08+nmu1) unstable; urgency=medium

  [ Helmut Grohne ]
  * Non-maintainer upload.
  * Mark bf-utf-source Multi-Arch: foreign. (Closes: #912195)

 -- Frédéric Bonnard <frediz@debian.org>  Fri, 27 Aug 2021 12:03:10 +0200

bf-utf (0.08) unstable; urgency=medium

  * Update Vcs-* fields to salsa.debian.org
  * Change Priority from extra to optional
  * Standards-Version: 4.1.4
  * Use debhelper 11
  * debian/copyright: Add machine-readable copyright format

 -- Changwoo Ryu <cwryu@debian.org>  Wed, 13 Jun 2018 03:49:46 +0900

bf-utf (0.07) unstable; urgency=medium

  * Remove Suggests on non-existent packages and simplify description
    (Closes: #695880)
  * Correct Vcs-* fields
  * Standards-Version: 3.9.5

 -- Changwoo Ryu <cwryu@debian.org>  Sat, 15 Mar 2014 23:17:18 +0900

bf-utf (0.06) unstable; urgency=low

  * Added Vcs-Browser and Vcs-Git fields.
  * Standard-Version: 3.9.3
  * Added debhelper to Build-Depends for clean target.
  * Use debhelper 9.
  * Use source format 3.0 (native)

 -- Changwoo Ryu <cwryu@debian.org>  Sat, 19 May 2012 23:21:58 +0900

bf-utf (0.05-0.1) unstable; urgency=low

  * Non-maintainer upload
  * Complete Persian characters so that D-I displays all characters
    for this language. Thanks to Eugeniy "Magic" Meshcheryakov for the patch.
    Closes: #323645

 -- Christian Perrier <bubulle@debian.org>  Sun, 11 Sep 2005 16:22:45 +0200

bf-utf (0.05) unstable; urgency=low

  * New maintaier
  * Incorporate NMU (Closes: #247822)

 -- Changwoo Ryu <cwryu@debian.org>  Sun, 22 May 2005 20:33:11 +0900

bf-utf (0.04-0.1) unstable; urgency=low

  * Non-maintainer upload.
  * Replaced unifont.bdf Hangul (Korean) glyphs with ones in Baekmuk Gulim
    font in http://chem.skku.ac.kr/~wkpark/baekmuk/iso10646/uglim14.bdf.gz
    (closes: #247872).  These are better matched with other glyphs in
    unifont.bdf.  The replaced Unicode regions are:
    (1) precomposed Hangul syllables (U+AC00 - U+D7A3)
    (2) compatibility jamos (U+3131 - U+318E)
    (3) a part of Enclosed CJK letters and months (U+3200 - U+32FF)

 -- Changwoo Ryu <cwryu@debian.org>  Fri, 28 May 2004 11:19:24 -0300

bf-utf (0.04) unstable; urgency=low

  * Changed debian/control (closes: #131990)

 -- Nicolas Sabouret <nico@debian.org>  Fri,  2 Apr 2004 17:33:02 +0200

bf-utf (0.03) unstable; urgency=low

  * Changed description (closes: #223141)
  * Minor corrections to the copyright

 -- Nicolas Sabouret <nico@debian.org>  Tue, 16 Dec 2003 11:26:28 +0100

bf-utf (0.02) unstable; urgency=low

  * New Maintainer (closes: #93561, #98666)
  * Correction of the copyright

 -- Nicolas Sabouret <nico@debian.org>  Fri,  5 Oct 2001 08:58:18 +0200

bf-utf (0.01) unstable; urgency=low

  * Initial Release.

 -- David Whedon <dwhedon@debian.org>  Sun, 11 Mar 2001 19:43:29 -0800
